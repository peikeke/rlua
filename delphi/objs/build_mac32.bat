@echo off
setlocal enabledelayedexpansion
set extra_path=..\..\..\src
for /f "tokens=*" %%x in (files.txt) do set sources=%%x
set files= 
for %%a in (%sources%) do set files=!files! %extra_path%\%%a
if not exist "mac32\" mkdir mac32
cd mac32
set flags=-target i686-macos -c -O3

::compilation
clang %flags% %files%

cd ..

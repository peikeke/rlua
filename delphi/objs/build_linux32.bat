@echo off
setlocal enabledelayedexpansion
set extra_path=..\..\..\src
for /f "tokens=*" %%x in (files.txt) do set sources=%%x
set files= 
for %%a in (%sources%) do set files=!files! %extra_path%\%%a
if not exist "linux32\" mkdir linux32
cd linux32
set flags=-target i686-linux -c -O3

::compilation
clang %flags% %files%

cd ..


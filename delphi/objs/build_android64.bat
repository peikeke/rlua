@echo off
setlocal enabledelayedexpansion
set extra_path=..\..\..\src
for /f "tokens=*" %%x in (files.txt) do set sources=%%x
set files= 
for %%a in (%sources%) do set files=!files! %extra_path%\%%a
if not exist "android64\" mkdir android64
cd android64
set flags=-target armv8a-linux-android -c -O3

::compilation
clang %flags% %files%

cd ..

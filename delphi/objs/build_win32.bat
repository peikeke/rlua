@echo off
setlocal enabledelayedexpansion
for /f "tokens=*" %%x in (files.txt) do set sources=%%x
if not exist "win32\" mkdir win32
set extra_path=..\..\..\src
cd win32
set flags=-target i686-win32 -c -O3
set files= 
::compilation
for %%a in (%sources%) do set files=!files! %extra_path%\%%a


clang %flags% %files%

cd ..

PAUSE
